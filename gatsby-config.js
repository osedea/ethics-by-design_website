/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: "Ethics by Design - 2020",
    siteUrl: "https://2020.ethicsbydesign.fr",
    twitter: "https://twitter.com/designethique",
    description: "Comment concevoir de façon responsable des services ou des produits numériques ? Telle est la question centrale que nous posons édition après édition à Ethics by design. Une question toujours centrale dans une année 2020 largement inédite tant par les évènements mondiaux que nous affrontons que par l’engouement général du monde de la conception numérique pour cette problématique. Au travers de conférences et ateliers donnés par des professionnels du design et chercheurs, vous allez pouvoir explorer le sujet de la conception repsonsable au travers du design, de l’économie, de la stratégie, de la formation, de la politique d’innovation.",
    author: "Association Designers Ethiques",
    social: [
      {
        type: "slack",
        url: "https://slack.designersethiques.org",
        icon: "/assets/slack.svg",
      },
      {
        type: "linkedin",
        url: "https://www.linkedin.com/company/designers-ethiques/",
        icon: "/assets/linkedin.svg",
      },
      {
        type: "twitter",
        url: "https://twitter.com/designethique",
        icon: "/assets/twitter.svg",
      },
      {
        type: "mastondon",
        url: "https://mastodon.design/@designersethiques",
        icon: "/assets/mastondon.svg",
      },
    ],
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `posts`,
        path: `${__dirname}/data/posts`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/data`,
        ignore: `**/*`,
      },
    },
    `gatsby-transformer-remark`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-react-helmet`
  ],
}
