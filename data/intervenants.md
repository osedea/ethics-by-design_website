---

title: Intervenant.es
path: /intervenants
speakers:
    - name: Agnès Crépet
      role: Responsable logiciel de Fairphone
      description: _
      picture_url: /images/agnes_crepet.jpg
      links:
        - target: https://twitter.com/agnes_crepet
          text: twitter
        - target: https://www.linkedin.com/in/agnescrepet/
          text: linkedin
    - name: Yaprak Hamarat
      role: Designer et chercheuse
      picture_url: /images/yaprak_hamarat.jpg
      links:
        - target: https://www.linkedin.com/in/yaprakhamarat/
          text: linkedin
    - name: Sabrina Tarquini
      role: UX & service designer chez Namahn
      picture_url: /images/sabrina_tarquini_5.jpg
      links:
        - target: https://www.linkedin.com/in/sabrinatarquini/
          text: linkedin
        - target: https://twitter.com/sabrina_trq
          text: twitter
    - name: Jacques-François Marchandise
      role: Délégué général de la Fing
      picture_url: /images/jacques_marchandise.jpg
      links:
        - target: https://twitter.com/jfmarchandise
          text: twitter
        - target: https://www.linkedin.com/in/jmarchandise/
          text: linkedin
    - name: Adèle Chasson
      role: Responsable des affaires publiques chez HOP - Halte à l'Obsolescence Programmée
      picture_url: /images/adele_hop.jpg
      links:
        - target: https://www.linkedin.com/in/ad%C3%A8le-chasson-2296ab120/
          text: linkedin
    - name: Céline Lescop
      role: Membre du Shift Project & Lead Digital Sustainability & Data Architect chez AXA Group Operations
      picture_url: /images/celine_lescop.jpeg
      links:
        - target: https://twitter.com/CelineLescop
          text: twitter
        - target: https://www.linkedin.com/in/celinelescop/
          text: linkedin
    - name: Alexandre Monnin
      role: Chercheur & directeur de la recherche de Origens Medialab
      picture_url: /images/alexandre_monnin.jpg
      links:
        - target: https://twitter.com/aamonnz
          text: twitter
        - target: https://www.linkedin.com/in/alexandremonnin/
          text: linkedin
    - name: Julien Borie
      role: Enseignant design de produit DSAA design éco-responsable Raymond Loewy
      picture_url: /images/julien_borie.jpg
      links:
        - target: https://www.linkedin.com/in/borie-julien-7471684b
          text: linkedin
    - name: Tony Fry
      role: Philosophe & designer
      picture_url: /images/tony_fry.jpg
      links:
        - target: https://www.linkedin.com/in/%C2%93tony-fry%C2%94-b0539b15/
          text: linkedin
    - name: Pierre-Yves Gosset
      role: Directeur de l'association Framasoft
      picture_url: /images/pierre_yves.jpeg
      links:
        - target: https://twitter.com/pyg
          text: twitter
        - target: https://www.linkedin.com/in/pygosset/
          text: linkedin
    - name: Collectif Bam
      picture_url: /images/illustration.jpg
      links:
        - target: https://twitter.com/CollectifBam
          text: twitter
    - name: Fairness
      picture_url: /images/illustration.jpg
      links:
        - target: https://twitter.com/fairnesscoop
          text: twitter
    - name: Solène Manouvrier
      role: Project management, recherche et facilitation Ouishare   
      picture_url: /images/solene_manouvrier.jpeg
      links:
        - target: https://twitter.com/Ouishare_Fr
          text: twitter
    - name: Pauline Rochart
      role: Consultante indépendante spécialiste des pratiques d’intelligence collective
      picture_url: /images/pauline.jpg
      links:
        - target: https://twitter.com/paurochart
          text: twitter
        - target: https://www.linkedin.com/in/pauline-rochart-4733894b/
          text: linkedin
    - name: Yaël Benayoun
      role: Co-fondatrice du Mouton Numérique
      picture_url: /images/yael.jpg
      links:
        - target: https://twitter.com/Geensly
          text: twitter
        - target: https://www.linkedin.com/in/yael-benayoun-271a05b5/
          text: linkedin
    - name: Irénée Régnauld
      role: Co-fondateur du Mouton Numérique et auteur de Mais Où Va Le Web
      picture_url: /images/irenee.jpg
      links:
        - target: https://twitter.com/IreR1
          text: twitter
        - target: https://www.linkedin.com/in/ireneeregnauld/
          text: linkedin
    - name: Célia Hodent
      role: Game UX Consultant
      picture_url: /images/celia_hodent.jpg
      links:
        - target: https://twitter.com/CeliaHodent
          text: twitter
        - target: https://www.linkedin.com/in/celiahodent/
          text: linkedin
    - name: Daniel Kaplan
      role: Co-fondateur Plurality University
      picture_url: /images/daniel.jpg
      links:
        - target: https://twitter.com/kaplandaniel
          text: twitter
    - name: Adrien Montagut
      role: co-fondateur de Commown
      picture_url: /images/adrien.jpg
      links:
        - target: https://twitter.com/AdrienMontagut
          text: twitter
    - name: Philippe Vion Dury
      role: Rédacteur en chef de Socialter
      picture_url: /images/philippe.jpg
      links:
        - target: https://twitter.com/PhilGood_Inc
          text: twitter
---
