---

title: Equipe d'organisation Ethics by Design
path: /organisateurs

organizers:
    - name: Karl Pineau 
      role: Doctorant et co-président Designers Ethiques
      description: Karl Pineau est doctorant en sciences de l’information et de la communication. Il s’intéresse particulièrement aux notions de design de l’attention, design persuasif.
      picture_url: /images/karl.jpg
      links:
        - text: linkedin
          target: https://www.linkedin.com/in/karlpineau/
        - text: twitter
          target: https://twitter.com/KarlPineau
    - name: Mellie La Roque
      role: Designer Stratégie, Systémie et UX, co-présidente Designers Ethiques 
      description: Mellie La Roque s'intéresse particulièrement au sujet de responsabilité du designer, de pratiques et méthodologies de design plus vertueuses - design systémique, sobriété numérique. 
      picture_url: /images/mellie.jpg
      links:
        - text: linkedin
          target: https://www.linkedin.com/in/mellie-la-roque-764bb773/
        - text: twitter
          target: https://twitter.com/mellie_la_roque
    - name: Dominique Karadjian
      role: Prospectiviste
      description: Chargée de veille stratégique qui s'intéresse de très près aux signaux faibles et aux tendances de l'anthropocène et de la privacy.
      picture_url: /images/dominique.jpg
      links:
        - text: linkedin
          target: https://www.linkedin.com/in/dominiquekaradjian/
        - text: twitter
          target: https://twitter.com/rackim
    - name: Marie-Cécile Paccard
      role: Designer systémique et UX indépendante
      description: Designer systémique et UX indépendante, j’agis pour un design inclusif et conscient des enjeux imposés par l’Anthropocène avec common-futures.org.
      picture_url: /images/mcgp.jpg
      links:
        - text: linkedin
          target: https://www.linkedin.com/in/mcpaccard/
        - text: twitter
          target: https://twitter.com/mcgodwinpaccard
    - name: Raphaël Yharrassarry
      role: Lead UX Designer
      description: En tant que Lead UX designer, psychologue en freelance, je conçois, depuis le dernier millénaire, des expériences utilisateurs mémorables.
      picture_url: /images/raphael.jpg
      links:
        - text: linkedin
          target: https://www.linkedin.com/in/raphaelyharrassarry/
        - text: twitter
          target: https://twitter.com/iergo

contributors:
    - name: Hubert Guillaud
      role: rédacteur en chef de InternetActu.net
      description: _
      picture_url: /images/hubertguillaud.jpg
      links:
        - text: twitter
          target: https://twitter.com/hubertguillaud
    - name: Irénée Regnauld
      role: bloggueur de Mais Où Va Le Web & co-fondateur du Mouton Numérique
      picture_url: /images/irenee.jpg
      links:
        - text: linkedin
          target: https://www.linkedin.com/in/ireneeregnauld/
        - text: twitter
          target: https://twitter.com/IreR1
    - name: Thomas Thibault
      role: Designer et co-fondateur collectif BAM
      picture_url: /images/thomas-thibault.jpg
      links:
        - text: linkedin
          target: https://www.linkedin.com/in/thomas-thibault/
        - text: twitter
          target: https://twitter.com/Thibault_Thms
    - name: Estelle Hary
      role: designer à la CNIL et co-fondatrice de Design Friction
      picture_url: /images/estellehary.jpg
      links:
        - text: linkedin
          target: https://www.linkedin.com/in/estelle-hary/
        - text: twitter
          target: https://twitter.com/EstelleHary
    - name: Régis Chatellier
      role: chargé d'études prospectives à la CNIL
      picture_url: /images/regischatellier.jpg
      links:
        - text: linkedin
          target: https://www.linkedin.com/in/regischatellier/
        - text: twitter
          target: https://twitter.com/el_Reg
    - name: Gauthier Rousshile
      role: designer
      picture_url: /images/gauthier_roussilhe_500.jpg
      links:
        - text: twitter
          target: https://twitter.com/AsWalterRobin
    - name: Julien de Sanctis
      role: doctorant & philosophe
      picture_url: /images/juliendesanctis.jpg
      links:
        - text: linkedin
          target: https://www.linkedin.com/in/julien-de-sanctis-8a418979/
        - text: twitter
          target: https://twitter.com/JulienDeSanctis
          
volunteers:
    - name: Marcelo Sharlau Coelho
    - name: Nicolas Borri

skillsPatrons:
    - name: / OSEDEA
      description: OSEDEA est une entreprise créant des solutions numériques sur mesure, simples et pratiques. Entre leurs mains, le code devient un art !
      picture_url: /images/osedea.png
      links:
        - text: website
          target: https://osedea.com
    - name: Le Laptop
      description: Depuis 2012, Le Laptop est né de la volonté de questionner le travail, sa valeur, son sens, ses produits, et son environnement. Composé en écosystème - Space, People, Methods - Le Laptop fédère une large communauté en quête de sens grâce à des évènements sur le rôle et responsabilités du Designer, des sprints d’innovation sociale et des formations certifiantes au Design centré utilisateur. Le Laptop réunit également un collectif de freelances dans son espace du 19e arrondissement et sur sa plateforme d’échanges.
      picture_url: /images/logo-laptop.png
      links:
        - text: website
          target: https://lelaptop.com
    - name: MURAL
      description: MURAL is a digital workspace for visual collaboration.Their platform and services enable innovative teams to think and collaborate visually to solve important problems. People benefit from MURAL’s speed and ease of use in creating diagrams, which are popular in design thinking and agile methodologies, as well as tools to facilitate more impactful meetings and workshops.
      picture_url: /images/logo-mural.png
      links:
        - text: website
          target: https://mural.co
    - name: Flupa
      description: Flupa est l’association francophone des professionnels de l’expérience utilisateur qui a pour objectif de créer un réseau de professionnels et promouvoir la pratique de l’expérience utilisateur (UX).
      picture_url: /images/logo_flupa.png
      links:
        - text: website
          target: http://flupa.eu
    - name: Zoom
      description: 
      picture_url: /images/zoom.jpg
      links:
        - text: website
          target: http://zoom.us


moneyPatrons:
     - name: DINUM
       picture_url: /images/logo-dinum.png
       description: La DINUM accompagne les ministères dans leur transformation numérique, conseille le gouvernement et développe des services et ressources partagées. Elle pilote le programme TECH.GOUV. La DINUM est un service du Premier ministre, placé sous l’autorité du ministre de l’Action et des Comptes publics, et mis à la disposition du ministre de l’Économie et des Finances et du secrétaire d’État chargé du Numérique. En son sein, le pôle Design des services numériques appuie les ministères pour une meilleure inclusion des usagers, et pour l’amélioration de l’expérience des services publics numériques pour tous, pour des services qui fonctionnent et sont simples, réactifs, attentifs, ludiques, bienveillants et humains.
       links:
         - text: website
           target: https://www.numerique.gouv.fr/
         - text: TECH.gouv
           target: https://www.numerique.gouv.fr/publication/tech-gouv-strategie-et-feuille-de-route-2019-2021/
     - name: Adeo
       picture_url: /images/logo-adeo.png
       description: ADEO est une communauté d’entreprises à taille humaine, ouvertes et interconnectées. La communauté ADEO, c’est 120 000 collaborateurs qui chaque jour, créent, accompagnent et rendent accessible leur rêve d’habitat aux habitants du monde. Notre force, notre moteur, c’est de nous sentir utiles dans tout ce que nous entreprenons, utile à soi, autour de soi, aux autres et au monde.
       links:
         - text: website
           target: https://www.adeo.com/
     - name: BackStory
       picture_url: /images/logo-backstory.png
       description: BACKSTORY est une agence de conseil indépendante fondée en 2010 par des spécialistes de l’expérience utilisateur et du conseil stratégique, engagés et soucieux d’appliquer une démarche éthique et responsable, dans un esprit entreprenarial.
       links:
         - text: website
           target: http://www.backstory.fr/fr/
     - name: WelcomeMax
       picture_url: /images/logo-welcomemax-bleu.png
       description: Agence conseil en design d’expérience UX/CX. L’ère digitale a profondément changé la manière dont les gens vivent et travaillent. Nous aidons les organisations à identifier les opportunités d’innovation et d’optimisation des dispositifs à l’ère des services connectés.
       links:
         - text: website
           target: https://welcomemax.com/

---

Ethics By Design est porté par **l’association Designers Éthiques**, une structure indépendante de **recherche-action autour des sujets de la responsabilité et de l’éthique au travers du design.**

Lieu de rencontre pour des professionnels, chercheurs, étudiants, nous leur offrons un cadre pour s’interroger sur leurs pratiques et la transition numérique de notre société dans le but d’**inventer un avenir durable et responsable.**

https://designersethiques.org
