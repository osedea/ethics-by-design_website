---
order: 2
title: Economie et responsabilite
titleComplet: Economie et responsabilite des entreprises
pictureUrl: /images/2-billet.png
pictureAlt: Un billet de 5 euros en gros plan
type: event
path: /programme
---

La responsabilité ne concerne pas seulement les designers, mais toute la stratégie des entreprises. En insufflant une réflexion éthique dans la conception, la transformation concerne également les modèles économiques et stratégiques. Le design associé à une vision économique vertueuse peut s’avérer être un puissant levier pour une transformation tournée vers un avenir socialement et écologiquent soutenable.

Comment concilier viabilité économique et engagement social et environnemental ? Quels modèles économiques permettent de concilier pérennité et durabilité ? Quels modèles alternatifs de gouvernance existent-ils ?

Nous interrogerons des designers pensant l’apport stratégique du design pour penser de nouveaux modèles d’entreprises, des entrepreneurs explorant de nouvelles formes d’organisations, des agences redéfinissant leur rôle et utilité dans la société.
