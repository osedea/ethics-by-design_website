---
order: 4
title: Politiques de l'innovation
titleComplet: Politiques de l'innovation
type: event
pictureUrl: /images/4-crosswalk.png
pictureAlt: Une photo d'une zone piétonne pleine de monde
path: /programme
---

Ces dernières décennies ont vu le remplacement du mot "progrès" par le mot "innovation". Une innovation réclamée de tous bords, dans les appels d'offre, dans les projets de recherche, dans les discours politiques. Mais pourquoi innover ? Pourquoi doit-on nécessairement produire quelque chose de nouveau (sens originel du terme innovation) ? Les choix techniques de notre société nous amènent à dire qu'il y a souvent un impenser de l'innovation, qu'elle est mobilisée comme une prémisse mais sans être jamais questionnée. 

Telle est donc la question que nous souhaitons poser dans cette thématique d'Ethics by design : pourquoi innover ? Vers quelle innovation devons-nous aller ? Comment penser l'innovation ? Et comment la conception peut aider à mieux définir les objectifs d'une innovation responsable ?

Nous interrogerons des acteurs des politiques publiques, des technocritiques, des acteurs oeuvrant pour l’intérêt général pour répondre à ces questions.
