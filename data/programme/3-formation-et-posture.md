---
order: 3
title: Formation et posture
titleComplet: Formation et posture du designer
type: event
pictureUrl: /images/3-student.png
pictureAlt: Une étudiante portant des livres
path: /programme
---

La formation est un moment clef dans le parcours d’une conceptrice ou d’un concepteur. Pourtant, trop peu d’écoles proposent dans leur parcours des moments et des cours dédiés à la responsabilité et à la posture du concepteur pour penser son impact et le rôle de la pratique du design pour le bien commun.

Comment mettre en oeuvre une approche de l'éthique dans les programmes des écoles, mais aussi dans leurs modes de gouvernances ? Et une fois sa formation achevée, comment mettre en pratique ses valeurs quand l’objectif premier d’un·e indépendant·e comme d’un·e salarié·e en agence reste la génération d’un revenu ? Comment trouver le juste équilibre entre projets qui aient du sens et projets qui permettent de vivre ?

Nous interrogerons des designers, qui cultivent une éthique du faire et de penser au travers de leur pratique - des enseignants qui proposent une approche consciente du métier de designer. Nous parlerons déontologie, posture éthique, responsabilité.
