---
path: /programme
type: lieu
title: Ethics by design
description: Ethics By Design se déroulera en ligne du 28 Septembre au 2 Octobre. La participation à l'évènement est à prix libre grâce au soutien de nos mécènes.
media: /images/cci-grand-lille.jpg

---
