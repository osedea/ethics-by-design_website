---
title: Programme
path: /programme
type: programmes
titleProgrammes: Une semaine thématique Ethics By Design
titlePrixConseil: Un évènement en ligne à prix conseillé
programmes:
  - jourTitle: Lundi 28 Sept.
    horaire:
      - heure: 14:00 - 16:00
        sessions:
        - type: Atelier en ligne
          categorie: Impact environnemental de la conception
          title: Comment concevoir avec sobriété ?
          intervenants: Low Tech Magazine
        - type: Atelier en ligne
          categorie: Impact environnemental de la conception
          title: Economie de la fonctionnalité, comment designer des outils et des services high-tech (dégafamisation, politique, réparabilité, etc.)
          intervenants: Adrien Montagut, coopérative Commown

      - heure: 16:00 - 16:30
        type: Ethics By Design live
        title: Introduction de l'évènement

      - heure: 16:30 - 17:30
        sessions:
        - type: Keynote
          title: Concevoir de manière responsable, l'exemple de Fairphone
          intervenants: Agnès Crépet et Alix Dodu

      - heure: 17:45 - 18:20
        sessions:
        - type: Conférence
          categorie: Impact environnemental de la conception
          title: Le choix des formes peut-il être un levier pour l’engagement écologique ?
          intervenants: Yaprak Hamarat

      - heure: 18:20 - 18:55
        sessions:
        - type: Conférence
          categorie: Impact environnemental de la conception
          title: Systemic design, a collaborative approach for systems change
          intervenants: Sabrina Tarquini

      - heure: 18:55 - 19:10
        type: Ethics By Design live
        title: Conclusion de la première journée

  - jourTitle: Mardi 29 Sept.
    horaire:
      - heure: 14:00 - 16:00
        sessions:
        - type: Atelier en ligne
          categorie: Formation et posture du designer
          title: Prospective, déconstruire nos croyances pour inventer de nouveaux futurs grâce aux artistes et à la fiction
          intervenants: Daniel Kaplan, Université de la Pluralité & Estelle Hary, Design Friction
        - type: Atelier en ligne
          categorie: Formation et posture du designer
          title: Comment monter une école de design avec des valeurs éthiques ?
          intervenants: Institut du design de Saint-Malo

      - heure: 16:00 - 16:15
        type: Ethics By Design live
        title: Retours sur la thématique "Impact environnemental de la conception"

      - heure: 16:15 - 17:45
        sessions:
        - type: Table-ronde
          categorie: Impact environnemental de la conception
          title: Quels enjeux politiques pour l’éco-conception de service numérique ?
          intervenants: Philippe Vion Dury (Socialter, animateur), Jacques-François Marchandise (FING), Adèle Chasson (Association HOP), Céline Lescop (Shift Project)

      - heure: 18:00 - 18:15
        sessions:
        - type: Entretien
          categorie: Politiques de l'innovation
          title: Entretien avec Charles-Pierre Astolfi, secrétaire général du Conseil National du Numérique
          intervenants: Charles-Pierre Astolfi

      - heure: 18:15 - 19:15
        sessions:
        - type: Format participatif
          categorie: Formation et posture du designer
          title: Regards croisés sur la pratique professionnelle du design, la parole aux étudiants
          intervenants: Roxane Célerier, Victor Ecrement, Cédric Fettouche & Antonio Zavaleta

      - heure: 19:15 - 19:30
        type: Ethics By Design live
        title: Conclusion de la deuxième journée

  - jourTitle: Mercredi 30 Sept.
    horaire:
      - heure: 14:00 - 16:00
        sessions:
        - type: Atelier en ligne
          categorie: Économie et responsabilité des entreprises
          title: Être designer au sein d'un collectif ?
          intervenants: Aurélie Jallut & Sophie Rocher (HappyDev)
        - type: Atelier en ligne
          categorie: Économie et responsabilité des entreprises
          title: Le label, une bonne solution ?
          intervenants: Demain Sera _ (Nicolas Enjalbert, Mael Inizan, Pierre-Yvon Carnoy)

      - heure: 16:00 - 16:15
        type: Ethics By Design live
        title: Retour sur la thématique "Formation et posture du designer"

      - heure: 16:15 - 17:45
        sessions:
        - type: Table-ronde
          categorie: Formation et posture du designer
          title: Comment créer des formations pronant la responsabilité ? 
          intervenants: Mellie La Roque (animation), Alexandre Monnin Master Antropocène, Julien Borie et Laurence Pache DSAA de La Souterraine, Aurélie Leclercq les Sismo

      - heure: 18:00 - 19:00
        sessions:
        - type: Keynote
          title: Économie et design, une pratique de redirection
          intervenants: Tony Fry

      - heure: 19:00 - 19:15
        type: Ethics By Design live
        title: Conclusion de la troisième journée
    
  - jourTitle: Jeudi 1 oct.
    horaire:
      - heure: 14:00 - 16:00
        sessions:
        - type: Atelier en ligne
          categorie: Politiques de l'innovation
          title: Comment penser l'innovation (via le design) pour qu'elle favorise l'intérêt général ? 
          intervenants: Agence La Formidable Armada
        - type: Atelier en ligne
          categorie: Formation et posture du designer
          title: Comment penser et rendre applicable une déontologie pour les designers ?
          intervenants: Marie-Cécile Godwin-Paccard & Karl Pineau

      - heure: 16:00 - 16:15
        type: Ethics By Design live
        title: Retour sur la thématique "Économie et responsabilité des entreprises"

      - heure: 16:15 - 16:50
        sessions:
        - type: Conférence
          categorie: Économie et responsabilité des entreprises
          title:  Framasoft, le modèle associatif est-il soluble dans la StartupNation ?
          intervenants: Pierre-Yves Gosset, co-directeur de l'association Framasoft

      - heure: 17:05 - 18:35
        sessions:
        - type: Table-ronde
          categorie: Économie et responsabilité des entreprises
          title: Les modèles alternatifs d'entreprises de design
          intervenants: Pauline Rochart (animation), Thomas Thibault (Collectif Bam), Solène Manouvrier (OuiShare), Hélène Maitre-Marchois (Coopérative Fairness)

      - heure: 18:35 - 19:10
        sessions:
        - type: Conférence
          categorie: Politiques de l'innovation
          title: Le design par les extrêmes users ou comment le handicap inspire l’innovation
          intervenants: Simon Houriez, directeur de l'association Signes de Sens

      - heure: 19:10 - 19:25
        type: Ethics By Design live
        title: Conclusion de la quatrième journée
    
  - jourTitle: Vendredi 2 oct.
    horaire:
      - heure: 14:00 - 16:00
        sessions:
        - type: Atelier en ligne
          categorie: Politiques de l'innovation
          title: Designer pour un territoire
          intervenants: Gauthier Roussilhe
        - type: Atelier en ligne
          categorie: Politiques de l'innovation
          title: Fresque du climat
          intervenants: Ripa Manukyan

      - heure: 16:00 - 16:15
        type: Ethics By Design live
        title: Retour sur la thématique "Politiques de l'innovation"

      - heure: 16:15 - 16:50
        sessions:
        - type: Conférence
          categorie: Politiques de l'innovation
          title: Eusko, monnaie locale, exemple de design de politique publique
          intervenants: Dante Edme-Sanjurjo


      - heure: 17:05 - 17:40
        sessions:
        - type: Conférence
          categorie: Politiques de l'innovation
          title: Comment faire entrer les technologies en démocratie ?
          intervenants: Irénée Regnauld, Yaël Benayoun

      - heure: 17:40 - 18:40
        sessions:
        - type: Keynote
          title: Éthique et jeux vidéo, entre magie et dark patterns
          intervenants: Célia Hodent

      - heure: 18:40 - 19:30
        type: Ethics By Design live
        title: Conclusion participative de l'évènement
    

---
