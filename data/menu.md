---

path: menu
tabs:
   - label: À propos
     url: /a-propos
     highlighted: false

   - label: Programme
     url: /programme
     highlighted: false

   - label: Intervenant·es
     url: /intervenants
     highlighted: false

   - label: Organisation
     url: /organisateurs
     highlighted: false

   - label: Blog
     url: /blog
     highlighted: false

   - label: Replays
     url: /replays
     highlighted: true

---

