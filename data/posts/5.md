---
path: "/blog-post/retour-dexperience-modele-economique"
date: "2020-10-26"
title: "Retour d'expérience #2 : un événement en ligne, quel modèle économique ?"
author: l'équipe d'organisation
toReadAlso:
    - path: ""
      title: "empty"
      author: "none"
---

Commençons par aborder l’aspect le plus incontournable d’un événement : son modèle économique. Et oui, nous aimerions ne pas avoir à nous en préoccuper, mais c’est une contrainte de taille !

## Le modèle d’un événement classique

Il faut commencer par expliquer ce qu’est le modèle économique d’un événement classique. En temps normal, lorsque nous réunissons des personnes en physique, il existe cinq grands types de dépense :

1. La location et la logistique du lieu, qui atteint vite des montants importants, de l’ordre de 30€ à 50€ par participant à l’événement (nous parlons toujours ici d’un prix pour 2 jours d’événement) ;
2. La restauration (buffets, cafés, viennoiserie), qui elle aussi atteint très vite 50€ par participant. Et c’est d’ailleurs toujours un sujet très délicat puisque classiquement un point de mécontement systématique du public, quelques soient les choix faits ;
3. Les défraiements, c’est-à-dire le paiement des billets de train, des hôtels pour les intervenants, voire pour certains le défraiement même de leur intervention. Sur un événement comme le nôtre, cela représente entre 2.000€ et 5.000€ selon le lieu de l’édition ;
4. La communication, qui inclut notamment la réalisation du site web, la captation des vidéos de replay, les vidéos de promotion, l’impression de kakémonos, etc.
5. Plus des dépenses diverses mais moins conséquentes comme le matériel d’atelier, les frais de billetterie, l’assurance, etc.

Ainsi, une place à un événement de 2 jours revient rapidement à une centaine d’euros en coût fixe, c’est-à-dire ce que consomme un participant du fait de sa présence. Centaine d’euros à laquelle il est nécessaire d’ajouter les frais qui eux décroissent proportionnellement avec le nombre de participants (par exemple la captation vidéo).

L’économie des événements en présentiel est donc très contrainte. Et c’est pour cela que ces événements sont soit ciblés à destination des professionnels et des entreprises (car elles ont les moyens de payer les places de leurs collaborateurs), soit co-portés par des acteurs publics sur le mode de la subvention. Et c’est seulement dans ce cas qu’il devient possible d’offrir un billet à tarif acceptable pour un public non professionnel.

De fait, ces événements sont très discriminants au plan économique, pas nécessairement parce qu’ils le souhaitent (en tout cas ça n’a jamais été notre cas), mais tout simplement parce que chaque participant induit un coût de base très élevé.

C’est d’autant plus vrai que nombre de personnes participent à l’événement sans payer de billet (les organisateurs et organisatrices bien sûr, mais également les intervenant·es, bénévoles, journalistes, invité·es, mécènes, celles et ceux bénéficiant de tarif réduit etc.) et que le coût de leur place est de fait déporté sur celles et ceux qui en payent une. Raison pour laquelle on arrive très vite à un prix de plusieurs centaines d’euros par place.

## Qu’est-ce qui change lors d’un événement en ligne ?
Eh bien tout change en réalité. Regardons d’un peu plus près le budget de l’édition 2020 de Ethics by design :

<table>
    <thead>
        <tr>
            <th align="left">Poste</th>
            <th align="right">Montant</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Restauration</td>
            <td align="right">900,00 €</td>
        </tr>
        <tr>
            <td>Logistique (location de mobilier, matériel sanitaire)</td>
            <td align="right">1 600,00 €</td>
        </tr>
        <tr>
            <td>Défraiements</td>
            <td align="right">2 900,00 €</td>
        </tr>
        <tr>
            <td>Communication</td>
            <td align="right">250,00 €</td>
        </tr>
        <tr>
            <td>Production audiovisuelle</td>
            <td align="right">17 500,00 €</td>
        </tr>
        <tr>
            <th align="left">TOTAL</th>
            <th align="right">23 150,00 €</th>
        </tr>
    </tbody>
</table>

Que constate-t-on de ce budget pour un événement numérique en distanciel ? Tout d’abord que certains postes budgétaires ont disparu. Par exemple, la location du lieu. Plus besoin d’accueillir du public, une salle d’une capacité de 30 personnes nous suffit désormais pour faire le live. D’ailleurs, merci à l’équipe du Laptop de nous avoir si bien accueilli pour l’ensemble de l'événement. Si vous ne connaissez pas le Laptop, courrez voir leur offre de formation en design, c’est l’une des meilleures qui soit !

Le budget de restauration a également fondu. Car évidemment, il n’y avait plus de participants à nourrir pendant 2 jours. Seuls étaient présents les intervenants et les organisateurs pendant les différentes journées de tournage et de live. Cette question de l’absence de restauration est d’ailleurs un point délicat. En effet, à chaque édition nous faisons notre possible pour faire appel à des traiteurs locaux & vertueux. Supprimer ce poste de dépense est de fait une coupe chez nos prestataires et une externalité négative de l’évolution du l’événement.

Côtés logistique, défraiement et communication, peu de changement. Si ce n’est que nous avons sorti de la communication tout ce qui relève de la production audiovisuelle pour en faire une section à part.

Car c’est ici la principale évolution de notre budget : la part de la production audiovisuelle a été multipliée par 4 par rapport à l’édition précédente. Cela s’explique aisément : capter, diffuser en live et en différé près de 18h de contenu audio-vidéo est inévitablement coûteux.

Si l’on rentre dans le détail, cette production se répartit de la façon suivante :

- 10.000€ de pré-production, c’est-à-dire enregistrer les différents formats avant la tenue de l'événement, ce qui inclut nos formats courts ;
- 5.500€ de production du live ;
- 1.500€ de frais de plate-forme de diffusion (c’est-à-dire l’abonnement à Zoom dans notre cas) ;
- 500€ de frais de plate-forme de replay (c’est-à-dire la mise en place de [notre instance Peertube](https://peertube.designersethiques.org)).

On en profite d’ailleurs pour remercier et vous recommander chaleureusement celles et ceux qui nous accompagnent dans notre production audiovisuelle : [La Monade Sagace](http://www.lamonadesagace.fr/), qui réalise depuis 2018 les captations de nos conférences et les formats courts que nous diffusons. Et [synchrone.tv](http://synchrone.tv/), la société de production des Parasites, qui a réalisé cette année les lives des conférences.

C’est d’ailleurs très important pour nous de travailler avec des gens que l’on apprécie, et de les financer à hauteur de nos moyens, et pas nécessairement selon le prix minimal que l’on peut négocier.

Ce coût de la production audiovisuelle était l’une de nos craintes lorsque nous avons fait le choix du distanciel. Nous n’avions aucune idée du coût d’un live. C’est aussi pour cela que nous vous présentons ces chiffres en toute transparence.

## Balance économique de l’événement numérique
Les recettes de l’édition s’élèvent à 34.000€, que l’on peut répartir entre 21.000€ de billetterie et 13.000€ de mécénat. Côté mécénat, cela veut dire que - comme à chaque édition - nous doublons nos recettes. Ce qui est un point très positif car cela montre que nous sommes dans une dynamique d’intérêt dont nos mécènes (qu’ils soient acteurs publics ou privés) sont conscients.

Du point de vue de la billetterie, c’est une toute autre histoire. Dans un événement classique où le prix serait en moyenne de 250€, cela veut dire que nous aurions vendu 85 petits billets. Pas de quoi sauter de joie.

Mais l’édition numérique rebat ici pleinement les cartes. Car comme vous pouvez le voir dans notre budget, nous n’avons plus aucune dépense liée aux participants : que 10 personnes ou 10.000 personnes participent revient peu ou prou au même. Nous avons supprimé le coût incompressible d’une place.

Ce qui veut dire que nous n’avons plus d’obligation à vendre un billet à prix élevé. Le prix du billet ne dépend plus de sa valeur économique mais de ce que chaque participant·e veut y mettre. C’est pourquoi nous avons choisi de mettre en place un billet à prix conseillé mais libre, de 50€.

On vous en dit plus sur ce choix et sur ces conséquences dans la suite de ce retour d’expérience. Rendez-vous la semaine prochaine pour le troisième article de ce retour d'expérience. Il sera consacré au choix du billet à prix libre que nous avons mis en place.
