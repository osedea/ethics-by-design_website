---

title: Replays
path: /replays
conferences:
    - name: Concevoir de manière responsable, l'exemple de Fairphone
      speaker: Agnès Crépet & Alix Dody
      links:
        - target: https://peertube.designersethiques.org/videos/watch/c5ac8499-556f-4c7a-acb9-36ea464beea2
          text: Accédez au replay de la conférence
    - name: Le choix des formes peut-il être un levier pour l’engagement écologique ?
      speaker: Yaprak Hamarat
      links:
        - target: https://peertube.designersethiques.org/videos/watch/de0210a2-9a57-4bc5-95d9-627c7086ad54
          text: Accédez au replay de la conférence
    - name: Systemic design, a collaborative approach for systems change
      speaker: Sabrina Tarquini
      links:
        - target: https://peertube.designersethiques.org/videos/watch/050bfaed-4e7d-4f15-a939-f297d3dd44d8
          text: Accédez au replay de la conférence
    - name: Quels enjeux politiques pour l’éco-conception de service numérique ?
      speaker: Philippe Vion Dury (Socialter, animateur), Jacques-François Marchandise (FING), Adèle Chasson (Association HOP), Céline Lescop (Shift Project)
      links:
        - target: https://peertube.designersethiques.org/videos/watch/134d2cda-98d4-4ca5-81a9-15aeee6d5961
          text: Accédez au replay de la conférence
    - name: Entretien avec Charles-Pierre Astolfi, secrétaire général du Conseil National du Numérique
      speaker: Charles-Pierre Astolfi
      links:
        - target: https://peertube.designersethiques.org/videos/watch/6011d3bb-0c09-4415-9498-a181555c1e67
          text: Accédez au replay de la conférence
    - name: Regards croisés sur la pratique professionnelle du design, la parole aux étudiants
      speaker: Roxane Célerier, Victor Ecrement, Cédric Fettouche & Antonio Zavaleta
      links:
        - target: https://peertube.designersethiques.org/videos/watch/2c8bea93-982d-488a-8070-af911795fcb4
          text: Accédez au replay de la conférence
    - name: Comment créer des formations pronant la responsabilité ?
      speaker: Mellie La Roque (animation), Alexandre Monnin Master Antropocène, Julien Borie et Laurence Pache DSAA de La Souterraine, Aurélie Leclercq les Sismo
      links:
        - target: https://peertube.designersethiques.org/videos/watch/d5853d28-1adf-4d46-8c9f-4ee75cb90329
          text: Accédez au replay de la conférence
    - name: Économie et design, une pratique de redirection
      speaker: Tony Fry
      links:
        - target: https://peertube.designersethiques.org/videos/watch/3f807289-1e8c-4d5a-8863-fd4042aadb64
          text: Accédez au replay de la conférence
    - name: Framasoft, le modèle associatif est-il soluble dans la StartupNation ?
      speaker: Pierre-Yves Gosset, co-directeur de l'association Framasoft
      links:
        - target: https://peertube.designersethiques.org/videos/watch/0e3b464a-3885-4ee4-af76-8b2e8952d548
          text: Accédez au replay de la conférence
    - name: Les modèles alternatifs d'entreprises de design
      speaker: Pauline Rochart (animation), Thomas Thibault (Collectif Bam), Solène Manouvrier (OuiShare), Hélène Maitre-Marchois (Coopérative Fairness)
      links:
        - target: https://peertube.designersethiques.org/videos/watch/eb8290f0-3738-4cbd-92da-3bfcc6191d3f
          text: Accédez au replay de la conférence
    - name: Le design par les extrêmes users ou comment le handicap inspire l’innovation
      speaker: Simon Houriez, directeur de l'association Signes de Sens
      links:
        - target: https://peertube.designersethiques.org/videos/watch/b4e43cdc-b051-4f5c-a612-20d32438ed6c
          text: Accédez au replay de la conférence
    - name: Eusko, monnaie locale, exemple de design de politique publique
      speaker: Dante Edme-Sanjurjo
      links:
        - target: https://peertube.designersethiques.org/videos/watch/2def6514-75d1-45fb-a02f-49a352df3919
          text: Accédez au replay de la conférence
    - name: Comment faire entrer les technologies en démocratie ?
      speaker: Irénée Regnauld, Yaël Benayoun
      links:
        - target: https://peertube.designersethiques.org/videos/watch/c3979cb2-440f-4aeb-bdd0-2e201811aa21
          text: Accédez au replay de la conférence
    - name: Éthique et jeux vidéo, entre magie et dark patterns
      speaker: Célia Hodent
      links:
        - target: https://peertube.designersethiques.org/videos/watch/2082d206-141a-446a-b4ce-f70f2ba03ef7
          text: Accédez au replay de la conférence

fullDays:
    - name: Journée 1 - Lundi 28 septembre
      speaker: Les Designers Ethiques
      links:
        - target: https://peertube.designersethiques.org/videos/watch/273cd958-0f33-45de-89e9-0e96748366f8
          text: Accédez au replay de cette journée
    - name: Journée 2 - Mardi 29 septembre
      speaker: Les Designers Ethiques
      links:
        - target: https://peertube.designersethiques.org/videos/watch/4a425a7f-8196-4ea9-a54a-7a75ead8999e
          text: Accédez au replay de cette journée
    - name: Journée 3 - Mercredi 30 septembre
      speaker: Les Designers Ethiques
      links:
        - target: https://peertube.designersethiques.org/videos/watch/0c682c9d-a27a-4925-b487-f8841cdd3729
          text: Accédez au replay de cette journée
    - name: Journée 4 - Jeudi 1 octobre
      speaker: Les Designers Ethiques
      links:
        - target: https://peertube.designersethiques.org/videos/watch/27d77753-d305-4124-9013-23557be49950
          text: Accédez au replay de cette journée
    - name: Journée 5 - Vendredi 2 octobre
      speaker: Les Designers Ethiques
      links:
        - target: https://peertube.designersethiques.org/videos/watch/f25b16f7-696e-4764-a8d3-901639fc135f
          text: Accédez au replay de cette journée
          
