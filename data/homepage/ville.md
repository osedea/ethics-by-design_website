---
path: "/"
title: Ethics By Design 2020
subtitle: Edition en ligne
media: /images/ethics_workshop.jpg
order: 2

mainsponsor: Avec le soutien exceptionnel de la DINUM

sponsors:
    -
        name: DINUM
        picture_url: /images/logo-dinum.jpg
        link: https://www.numerique.gouv.fr/
    -
        name: Adeo
        picture_url: /images/logo-adeo.png
        link: https://www.adeo.com/
    -
        name: BackStory
        picture_url: /images/logo-Backstory_w.png
        link: https://www.backstory.fr/
    -
        name: WelcomeMax
        picture_url: /images/wm-blanc-2.png
        link: https://welcomemax.com/
    -
        name: Le Laptop
        picture_url: /images/logo-laptop.png
        link: https://www.lelaptop.com/
    -
        name: OSEDEA
        picture_url: /images/logo-white-osedea.png
        link: https://www.osedea.com

soutien: avec le soutien de la cnil et la fing

---

Ethics By Design s’inscrit dans la programmation de Lille Métropole 2020, Capitale Mondiale du Design. Pour les raisons sanitaires, l’évènement ne pourra avoir lieu en physique mais à distance durant une semaine thématique.

<br />

![logo capital du design et](/images/lille-logos.png)
