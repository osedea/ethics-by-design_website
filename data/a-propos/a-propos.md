---

title: À Propos
path: /a-propos
order: 1

---

#Comment concevoir de façon responsable des services ou des produits numériques ?

Telle est la question que nous posons édition après édition à Ethics by design. Une question toujours centrale dans une année 2020 largement inédite tant par les évènements mondiaux que nous affrontons que par l’engouement général du monde de la conception numérique pour cette problématique.

###Où en sommes-nous de la responsabilité dans la conception ?

Ces dernières années ont fait émerger nombre de problématiques du conception plus ou moins inédites. La question de la privacy et du respect des données personnelles a longtemps été le principal angle par lequel la responsabilité a été abordée. L’accessibilité des services numériques est également un sujet historiquement abordé, notamment au sein de certaines communautés de développeurs.

Récemment nous avons nous-même largement traité de la question du design de l’attention et du design persuasif. Et aujourd’hui, c’est la question environnementale qui est au coeur de la plupart des initiatives. Nous pourrions aussi mentionner le travail mené ces dernières années autour du rapport entre numérique et démocratie, ou celui entre numérique et société.

Les champs de la conception responsable paraissent ainsi toujours plus nombreux, et les problématiques qu’ils soulèvent toujours plus importantes.

###2020, une année charnière ?

Face à cette explosion des champs de la conception responsable ; des initiatives associatives, universitaires, institutionnelles ou entrepreneuriales ; de la littérature autour de ces questions ; des initiatives étudiantes revendiquant une formation prenant en compte ces questions ; de la remise à l’honneur des textes et discours des pionniers de la technocritique et de la conception responsable ; n’assiste-t-on pas à un moment charnière de la conception en 2020 ? C’est en tout cas ce que nous appelons de nos voeux.

Mais pour éviter l’écueil d’un énième coup d’épée dans l’eau ou d’une vision “ethics-washing” de la conception responsable, il nous semble nécessaire d’affirmer collectivement ce que doit être cette conception numérique responsable et durable. C’est précisément le sens Ethics by design 2020.

###Ethics by design 2020 

Après Lyon en 2017 et Paris en 2018, Ethics by design 2020 se déroulera en ligne, en raison de la situation sanitaire, tout en s'inscrivant au sein de Lille Métropole 2020, Capitale Mondiale du Design. L'évènement est organisé en partenariat avec la métropole de Lille et son comité d’organisation La République du design.

Sur une semaine, Ethics by design permettra à ses participants de s’interroger sur les manières dont le design peut être une pratique de redirection promouvant la responsabilité au travers de la conception, de la stratégie, de l’économie : low tech, nouveaux modèles économiques des systèmes numériques, biens communs numériques ou encore plate-formisation de la société seront au coeur de cette semaine thématique.

Nous vous proposons d’aborder cette problématique à travers 4 thématiques qui rythmeront notre semaine d’échanges : impact environnemental de la conception, formation et posture du designer, économie et responsabilité des entreprises, & politiques de l’innovation.

###Interdisciplinarité et horizontalité

Ethics by design vous propose ainsi un fil de conférences en continu chaque jour de la semaine du 28 septembre, de 16h à 19h, permettant de traiter en profondeur nos thématiques via les prises de parole de designers, universitaires et personnalités de la société civile au travers d’un programme éditorial sans appel à participation.

Tout comme lors des éditions 2017 et 2018 de l’évènement, nous vous proposons en parallèle de participer à la construction des services de demain en traitant ces thématiques avec le plus d’horizontalité possible à travers des formats participatifs comme les ateliers, forcément moins évidents en ligne, mais que nous souhaitons tout de même maintenir.

Ce, afin de permettre à ceux qui constituent le public traditionnellement hétérogène d’Ethics by design de pouvoir dialoguer entre eux : designers, studios, agences, universitaires, journalistes, chefs de projet et consultants. 

**Alors, vous en êtes ?**