import React from "react"

import Layout from "../layouts/layout"
import Section from "../components/section"
import Title from "../components/title"

const Billetterie = ({ location }) => (
    <Layout location={location}>
        <Section>
            <Title />
            <strong>Informations à propos de la billetterie</strong>
            <ul>
                <li>Le prix conseillé est de 50€ pour l'accès à toutes les conférences (15h de conférence en tout). Vous pouvez bien sûr rajouter un peu plus si vous estimez que l'évènement en vaut la peine !</li>
            </ul>
            <iframe id="haWidget" src="https://www.helloasso.com/associations/les-designers-ethiques/evenements/ethics-by-design-2020/widget" style={{ width: '100%', minHeight: '800px', overflow: 'none', border: 0, marginTop: '50px' }}></iframe>
        </Section>
    </Layout>
)

export default Billetterie;
