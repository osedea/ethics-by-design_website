import React from "react"

import Layout from "../layouts/layout"
import Section from "../components/section"
import Title from "../components/title"
import Title2 from "../components/titleh2"
import {graphql} from "gatsby";
import ConferencesReplay from "../components/conferences-replay";


export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(filter: {frontmatter: {path: {eq: "/replays"}}}) {
        edges {
            node {
                frontmatter {
                    path
                    title
                    conferences {
                        name
                        speaker
                        links {
                            target
                            text
                        }
                    }
                    fullDays {
                        name
                        speaker
                        links {
                            target
                            text
                        }
                    }
                }
                html
            }
        }
    }
  }
`


const Replays = ({ location, data }) => {
    const replays = data.allMarkdownRemark.edges[0].node.frontmatter;

    return (
        <Layout location={location}>
            <Section>
                <Title>{replays.title}</Title>

                <Title2>Replay des journées entières :</Title2>
                <ConferencesReplay replays={replays.fullDays} />

                <div style={{ marginTop: '50px' }}></div>

                <Title2>Accès à une conférence spécifique :</Title2>
                <ConferencesReplay replays={replays.conferences} />
            </Section>
        </Layout>
    );
}

export default Replays;
