import React from "react"
import { graphql } from "gatsby"
import styled from "styled-components"

import Layout from "../layouts/layout"
import Hero from "../components/hero"
import Section from "../components/section"
import Title from "../components/title"
import JourProgramme from "../components/jour-programme"
import NumberedList from "../components/numbered-list"
import Card from "../components/card";

const CONTENT_TYPE = {
    EVENT: 'event',
    PROGRAMMES: 'programmes',
    PLACE: 'lieu',
};

export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(filter: {frontmatter: {path: {eq: "/programme" }}}) {
        edges {
            node {
                frontmatter {
                    path
                    title
                    titleProgrammes
                    titleComplet
                    titlePrixConseil
                    pictureUrl
                    pictureAlt
                    order
                    type
                    media
                    description
                    programmes {
                        jourTitle
                        horaire {
                            heure
                            title
                            type
                            sessions {
                                type
                                categorie
                                title
                                intervenants
                            }
                        }
                    }
                }
                html
            }
        }
    }
  }
`

const ProgrammeContainer = styled.div`
    display: flex;

    @media screen and (max-width: 968px) {
        flex-direction: column;
    }

    & > * > *:last-child {
        margin-bottom: 0;
    }
`;

const Programme = ({ location, data }) => {

    // Page title
    let title = '';
    let subtitle = '';
    let titlePrixConseil = '';
    // Array of events with full description
    const events = [];
    // Array of programmes days
    let programmes = [];
    let place = null;

    data.allMarkdownRemark.edges.forEach((edge) => {
        if (edge.node.frontmatter.type === CONTENT_TYPE.PROGRAMMES) {
            title = edge.node.frontmatter.title;
            subtitle = edge.node.frontmatter.titleProgrammes;
            titlePrixConseil = edge.node.frontmatter.titlePrixConseil;
            programmes = edge.node.frontmatter.programmes;
        } else if (edge.node.frontmatter.type === CONTENT_TYPE.EVENT) {
            events.push({
                order: edge.node.frontmatter.order,
                title: edge.node.frontmatter.titleComplet,
                pictureUrl: edge.node.frontmatter.pictureUrl,
                pictureAlt: edge.node.frontmatter.pictureAlt,
                text: edge.node.html
            });
        } else if (edge.node.frontmatter.type === CONTENT_TYPE.PLACE) {
            place = edge.node;
        }
    });

    return (
        <Layout location={location}>
            <Section>
                <Title>{title}</Title>
                <NumberedList items={events.sort((a, b) => parseFloat(a.order) - parseFloat(b.order))} />
            </Section>
            <Section>
                {subtitle && <Title>{subtitle}</Title>}
                <ProgrammeContainer>
                    {programmes.map((programme) => <JourProgramme key={programme.jourTitle} programme={programme} />)}
                </ProgrammeContainer>
            </Section>
            <Section noMarginTop>
                <Card
                    title="Partageons cet évènement ensemble !"
                    htmlContent="<div style='margin-bottom: 30px;'></div>"
                    linkText="Je prends mon billet"
                    linkTarget="/billetterie"
                    internalLink
                />
            </Section>
            <Section dark>
                <Hero
                    media={place.frontmatter.media}
                    sideTitle={place.frontmatter.title}
                    side={place.frontmatter.description}
                    dark
                />
            </Section>
        </Layout>
    );
}

export default Programme;
