import React from "react"
import styled from "styled-components"

const Container = styled.div`
    margin-left: 60px;
    margin-right: 60px;

    @media screen and (max-width: 968px) {
        margin-left: 0;
        margin-right: 0;
    }
`;

export default function Inset(props) {
    return (
        <Container style={props.style} {...props}>
            {props.children}
        </Container>
    )
};
