import React from "react"
import { Helmet } from "react-helmet"
import useSiteMetadata from "../hooks/use-site-metadata"

import favIcon from "../../static/favicon.ico"
import banniere from "../../static/images/banniere.jpg"

const SiteMetadata = ({ pathname }) => {
    const { siteUrl, title, twitter, description, author } = useSiteMetadata()

    // Note: `location.href` isn't available on server-side so we must get it from `pathname`:
    // https://css-tricks.com/how-to-the-get-current-page-url-in-gatsby/#article-header-id-4
    const href = `${siteUrl}${pathname}`

    return (
        <Helmet defer={false} defaultTitle={title} titleTemplate={`%s | ${title}`}>
            <html lang="fr" />
            <link rel="canonical" href={href} />
            <meta content="text/html; charset=utf-8" />
            <meta
                name="viewport"
                content="width=device-width,initial-scale=1,shrink-to-fit=no,viewport-fit=cover"
            />
            <link rel="icon" href={`${siteUrl}${favIcon}`} />

            <meta property="description" content={description} />
            <meta name="author" content={author} />
            <meta property="og:url" content={href} />
            <meta property="og:type" content="website" />
            <meta property="og:site_name" content={title} />
            <meta property="og:image" content={`${banniere}`} />
            <meta property="og:image:alt" content="Bannière Ethics by design" />
            <meta property="og:image:width" content="1000" />
            <meta property="og:image:height" content="422" />

            <meta name="twitter:card" content="summary" />
            <meta name="twitter:site" content={twitter} />
        </Helmet>
    )
}

export default SiteMetadata
