import React, { useState } from "react"
import { Link } from "gatsby"
import Logo from "./logo"

import style from "./header.module.css"

export default function Header(props) {
    const [isMobileMenuShown, showMobileMenu] = useState(false)

    return (
        <header className={style.header}>
            <div className={style.logoMenu}>
                <Logo />
                <button
                    className={style.mobileButton}
                    type="button"
                    onClick={() => showMobileMenu(!isMobileMenuShown)}
                />
            </div>
            <ul
                className={
                    isMobileMenuShown ? style.mobileMenuShown : style.mobileMenuHidden
                }
            >
                {props.pages.map((item) => (
                    <li
                        key={item.url}
                        className={
                            (props.location &&
                                props.location.pathname === item.url &&
                                style.active) ||
                            ""
                        }
                    >
                        <Link
                            to={item.url}
                            className={`${style.link} ${
                                item.highlighted && style.linkhighlighted
                                }`}
                        >
                            {item.label}
                        </Link>
                    </li>
                ))}
            </ul>
        </header>
    )
}
