import React from "react"

import style from "./jour-programme.module.css";

export default function JourProgramme(props) {
    return (
        <div className={style.jour}>
            <div className={style.jourTitle}>{props.programme.jourTitle}</div>
            <div className={style.schedule}>
                {/* Timeslot grid */}
                {props.programme.horaire &&
                    props.programme.horaire.map((timeSlotSchedule, index) => {
                        return timeSlotSchedule.heure && (
                            <div key={timeSlotSchedule.heure}>
                                {timeSlotSchedule.sessions
                                    ? <div className={style.sessionsBlock}>
                                        <div className={[style.time, style.timeBorder, timeSlotSchedule.sessions.length === 1 ? style.singleSession : ''].join(' ')}>{timeSlotSchedule.heure}</div>
                                        {timeSlotSchedule.sessions.map((session) => (
                                            <div key={`${props.programme.jourTitle}-${timeSlotSchedule.heure}-${session.title}`} className={style.timeSlot}>
                                                <div className={style.eventType}>
                                                    {session.type}
                                                </div>
                                                <div className={style.eventCategory}>
                                                    {session.categorie}
                                                </div>
                                                <div className={style.eventTitle}>
                                                    {session.title}
                                                </div>
                                                <div className={style.eventSpeaker}>
                                                    {session.intervenants}
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                    : <div className={style.liveBlock}>
                                        <div className={style.time}>{timeSlotSchedule.heure}</div>
                                        <div className={style.eventType}>
                                            {timeSlotSchedule.type}
                                        </div>
                                        <div className={style.eventCategory}>
                                            {timeSlotSchedule.title}
                                        </div>
                                    </div>
                                }
                            </div>
                        );
                    })
                }

                {/* Closing */}
                {props.programme.fermetureHeure &&
                    <div className={style.closing}>
                        <div className={style.time}>{props.programme.fermetureHeure}</div>
                        <div className={style.closingEventTitle}>{props.programme.fermetureTitle}</div>
                    </div>
                }
            </div>
        </div>
    )
};
