import React from "react"

import style from "./section-blog.module.css"

export default function SectionBlog(props) {
  return (
    <section className={[style.section, props.dark && style.black, props.noMarginTop && style.noMarginTop].join(' ')}>
      <div
        className={[style.container, props.vertical && style.vertical, props.horizontal && style.horizontal].join(
          ' '
        )}
      >
        {props.children}
      </div>
    </section>
  )
}
