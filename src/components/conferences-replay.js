import React from "react"

import Replay from "./replay"
import Grid from "./grid"

export default function ConferencesReplay(props) {
    return (
        <Grid columns={3} mobileColumns={2} offsetLeft>
            {props.replays.map((replay, index) => (
                <Replay
                    key={`replay-${index}`}
                    {...replay}
                />
            ))}
        </Grid>
    );
}
