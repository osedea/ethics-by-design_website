import React from "react"

import style from "./numbered-list.module.css"

import BoxedText from "./boxed-text"
import PrefixedImage from "./prefixed-image"
import Inset from "./inset"

export default function NumberedList(props) {
    return (
        <Inset className={style.numbered}>
            {props.items && props.items.length > 0 ?
                <ol start="0">
                    {props.items.map(
                            (item) => (
                                <li key={item.title}>
                                    <div className={style.columnContainer}>
                                        <div className={style.leftColumn}>
                                            <BoxedText className={style.title}>{item.title}</BoxedText>
                                            <PrefixedImage src={item.pictureUrl} className={style.image} alt={item.pictureAlt} />
                                        </div>
                                        <div className={style.rightColumn}>
                                            <div dangerouslySetInnerHTML={{ __html: item.text }} />
                                        </div>
                                    </div>
                                </li>
                            )
                        )
                    }
                </ol>
            : <p>Pas de contenu pour le moment.</p>}
        </Inset>
    )
};
