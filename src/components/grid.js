import React from "react"

import styled from "styled-components"

import Inset from "./inset"

const GridComponent = styled.div`
  display: grid;
  flex: 1;
  grid-template-rows: ${(props) => `repeat(${props.rows}, 1fr)`};
  grid-column-gap: 15px;
  grid-row-gap: 15px;

    @media screen and (max-width: 968px) {
        grid-template-columns: ${(props) => `repeat(${props.mobileColumns || props.columns}, 1fr)`};
    }
    @media screen and (min-width: 968px) {
        grid-template-columns: ${(props) => `repeat(${props.columns}, 1fr)`};
    }
`

export default function Grid(props) {
    const grid = (
        <GridComponent {...props}>
            {props.children}
        </GridComponent>
    );

    if (props.offsetLeft) {
        return (
            <Inset>
                {grid}
            </Inset>
        );
    }

    return grid;
}
