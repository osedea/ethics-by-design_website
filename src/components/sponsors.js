import React from "react"

import Sponsor from './sponsor';
import style from "./sponsors.module.css"
import PrefixedImage from "./prefixed-image"

export default function Sponsors(props) {
  return (
    <div className={style.sponsors}>
      <div className={style.main}>{props.main}</div>
      <ul className={style.list}>
        {props.items.map((item, index) => (
          <li key={index} className={style.li}>
            <a href={item.link} target="_blank">
                {!item.picture_url && item.name}
                {item.picture_url && <PrefixedImage src={item.picture_url} alt={item.name} className={style.logo} />}
            </a>
          </li>
        ))}
      </ul>
      <div className="sponsor-support">{props.support}</div>
    </div>
  )
}
