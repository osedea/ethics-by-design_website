import React from "react"
import PrefixedImage from "./prefixed-image"

import Tag from "./tag"

import style from "./replay.module.css"

export default function Replay(props) {
  return (
    <article className={[style.replay, props.dark && style.dark].join(' ')}>
        <div className={[style.headers, !props.cut && style.pullup].join(' ')}>
            <header>{props.name}</header>
        </div>
        {!props.cut ? (
            <div>
            <p className={style.description}>{props.speaker}</p>
            {props.links && props.links.length > 0
                ? <footer className={style.social}>
                    {props.links.map((link) => (
                        <Tag key={`${props.name}-${link.text}`} link={link.target} text={link.text} />
                    ))}
                </footer>
                : null
            }
            </div>
        ) : null}
    </article>
  )
}
