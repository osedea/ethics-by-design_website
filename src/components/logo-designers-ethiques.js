import React from "react"

import style from "./logo-designers-ethiques.module.css"

export default function Logo() {
  return (
    <a href="https://designersethiques.org">
        <div className={style.logo}>
          <span>designers</span>
          <span>éthiques</span>
        </div>
    </a>
  )
}
